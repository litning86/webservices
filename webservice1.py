import os
import sys
from flask import Flask, request
from flask_restful import Resource, Api

webservice1 = Flask(__name__)
api = Api(webservice1)

class getGenres(Resource):

    def put(self):
        file = request.files['files_text']
        uploads_dir = os.path.join(webservice1.instance_path, "upload")
        if os.path.isdir(uploads_dir) is False:
            os.makedirs(uploads_dir)
        file.save(os.path.join(uploads_dir, file.filename))


    def get(self):
        cwd = os.getcwd()
        with open('instance/upload/tempFile.txt', 'r') as f:
            movies = [line.strip() for line in f]

        genreCount = 0
        genreList = []
        initialCount = []
        percentageCount = []

        for item in movies:
            genre = item.rsplit("-", 1)[1]
            genreCount += 1
            if genre not in genreList:
                genreList.append(genre)
                newIndex = genreList.index(genre)
                initialCount.append(1)
            else:
                newIndex = genreList.index(genre)
                initialCount[newIndex] = initialCount[newIndex] + 1

        for item in initialCount:
            percentage = str(round(((float(item) / float(genreCount)) * 100), 2)) + "%"   #calculates the percentage
            percentageCount.append(percentage)                             #for each genre
        outputString = ""
        tempS = ""
        outputList = []
        for i in range(len(genreList)):
            #outputString = outputString + genreList[i] + ": " + percentageCount[i] + ",  "
            tempS = str(genreList[i] + ": " + str(percentageCount[i]))
            outputList.append(tempS)
        return outputList

api.add_resource(getGenres, '/uploadFile')

if __name__ == "__main__":
    webservice1.run(debug=True)
