Readme

Requirements:
Included with the project is a requirements.txt

Simply run:
python -m pip install --upgrade pip --user
python -m pip install -r requirements.txt --user

(when running in feng, if issues arise, install all pip requirements separately using
python -m pip install “requirement” --user)

Compilation Instructions:
In the main directory open 3 terminals
1st terminal run – python ./app.py
2nd terminal run – python ./webservice1.py
3rd terminal run – python ./WebService2.py

Runtime instructions:
browse to the ip in terminal 1 (http://127.0.0.5:3000/)
when asked to upload a file, upload the films.txt in the main directory.
(this file can be edited or any other txt can be used as long as it’s formatted the same and the only genre’s used are
Sci Fi
Horror
Drama
Fantasy
RomCom)

Select a genre and click submit
enjoy your youtube trailer.
