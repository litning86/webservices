from flask import Flask, render_template, redirect, request
import random
import os
import requests
import time
import json
from webservice3 import getJson

app = Flask(__name__)


@app.route('/')
def GetInput():
    return render_template('InputMovie.html')

@app.route('/movietrailer', methods=['POST', 'GET'])
def ChooseFilm():
    chosen = request.form['movie']
    #chosengenre = chosen.split(":")[0]
    #response = requests.get('http://127.0.0.3:4000/Films/' + chosengenre)
    #searchstring = response.json()
    x = getJson(chosen)
    y = x["items"][0]["id"]["videoId"]
    youtube_string = "https://www.youtube.com/embed/" + str(y)
    return render_template('Trailer.html', value = youtube_string)



if __name__ == '__main__':
    app.run(host='127.0.0.5', port=3000, debug=True, use_reloader=True)
