from flask_restful import Api, Resource
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import random

WebService2 = Flask(__name__)
api = Api(WebService2)
WebService2.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
db = SQLAlchemy(WebService2)

#Movie database model
class moviesdb(db.Model):
        id = db.Column(db.Integer, unique=True, primary_key=True)
        movieTitle = db.Column(db.String(40), unique=True)
        Genre = db.Column(db.String(15))

##API
class SuggestAFilm(Resource):
    def get(self, genre):
        Films = []
        Instances = moviesdb.query.filter_by(Genre=genre).all() #query the db for all instances of entries with a certain genre
        for x in Instances:
            Films.append(x.movieTitle)
        ranNo = random.randint(0, len(Films) - 1 )
        Suggestion = Films[ranNo]
        return {"data": Suggestion}

api.add_resource(SuggestAFilm, '/Films/<string:genre>')

if __name__ == "__main__":
    WebService2.run(host='127.0.0.3', port=4000, debug=True)
