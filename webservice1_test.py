from flask import Flask, render_template, redirect, request
import random
import os
import requests
import time
import json
from webservice3 import getJson

#stuff = requests.put("http://127.0.0.1:5000/uploadFile", files={'files_text': open('films.txt', 'r')})

#print(stuff)


#retrieveGenres = requests.get("http://127.0.0.1:5000/uploadFile")

#print(retrieveGenres)
app = Flask(__name__)

@app.route('/')
def GetInput():
    return render_template('FileForm.html')

@app.route('/submitted', methods=['POST', 'GET'])
def WebService1():
    t0 = time.time()
    file = request.files['file']
    file.save('tempFile.txt')
    uploadTextFile = requests.put("http://127.0.0.1:5000/uploadFile", files={'files_text': open('tempFile.txt', 'r')})
    retrieveGenres = requests.get("http://127.0.0.1:5000/uploadFile")
    t1 = time.time()
    print("TOTAL:  --->   " + str(t1-t0))
    print(retrieveGenres.json())
    return render_template('GenrePercentage.html', list=retrieveGenres.json())


if __name__ == '__main__':
    app.run(host='127.0.0.5', port=3000, debug=True, use_reloader=True)
