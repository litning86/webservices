import os
import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors
import json
from flask import Flask
from flask_restful import Resource, Api

def getJson(searchString):
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = "AIzaSyD18Akgj0IPVTw_J7TQoYKuOMEv5jGnJow"

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY)

    request = youtube.search().list(
        part="snippet",
        maxResults=1,
        q=searchString + " trailer"
    )
    response = request.execute()
    return response
