from flask import Flask, render_template, request
import random
import requests
import time
from webservice3 import getJson

app = Flask(__name__)

@app.route('/')
def GetInput():
    return render_template('FileForm.html')

@app.route('/submitted', methods=['POST', 'GET'])
def WebService1():
    file = request.files['file']
    file.save('tempFile.txt')
    uploadTextFile = requests.put("http://127.0.0.1:5000/uploadFile", files={'files_text': open('tempFile.txt', 'r')})
    retrieveGenres = requests.get("http://127.0.0.1:5000/uploadFile")
    return render_template('GenrePercentage.html', list=retrieveGenres.json())

@app.route('/MovieIdea', methods=['POST', 'GET'])
def ChooseFilm():
    chosen = request.form['option']
    chosengenre = chosen.split(":")[0]
    response = requests.get('http://127.0.0.3:4000/Films/' + chosengenre)
    searchstring = response.json()
    x = getJson(searchstring["data"])
    y = x["items"][0]["id"]["videoId"]
    youtube_string = "https://www.youtube.com/embed/" + str(y)
    return render_template('Trailer.html', value = youtube_string)


if __name__ == '__main__':
    app.run(host='127.0.0.5', port=3000, debug=True, use_reloader=True)

##<iframe src="https://www.youtube.com/embed/ayW2SS4VmGw width="853" height="480" frameborder="0" allowfullscreen></iframe>
