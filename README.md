# WebServices
Allows a user using a web client to upload a file of movies. The 1st Web service takes 
this file and returns a list of genres and its genre percentile. The user can then pick a 
genre which is supplied to the 2nd Web service, where a database of movies is queried 
a random film of the supplied genre is returned. The returned film title is the sent to 
the 3rd web service where it queries Youtube for video id to display an embedded 
video in the web client for the user to watch
