import requests
import time

list = ["Sci Fi", "Drama", "Horror", "RomCom"]

print("Please pick a genre")
print("1. Sci Fi")
print("2. Drama")
print("3. Horror")
print("4. RomCom")

input = input("pick a number:  ")
t0 = time.time()
response = requests.get('http://127.0.0.3:4000/Films/' + list[int(input) - 1])
t1 = time.time()
print("The Random Movie for you based on the inputed genre is " + response.json()["data"])
print(t1-t0)
